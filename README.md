xejail
======

`xejail` is a small binary designed to run increase the
safety of running arbitrary programs using modern Linux features.

The overall goal of `xejail` is to wrap various Linux features or
syscalls in a configurable interface. Configuration is specified
via command-line arguments (or can be put in a file, with the same format).
The command-line interface is designed to be used easily from other programs,
and as such is not very high level.


Usage
-----

`xejail` needs root privileges for most of the jailing operations (or
must be marked setuid).

Prefix the command to be run with `--`.
```sh
$ # as root
$ ./xejail -- ls /tmp
```

Options are specified before `--`.
```sh
$ ./xejail --allow_fork --allow_exec -- bash
```

By default, `xejail` will output run stats to stdout. Use `--result_file` to
change this.


Features
--------

Basic features:

 - Run as an unprivileged user (defaults to "nobody")
 - I/O redirection for stdio
 - Clean set of environment variables, with optional overrides
 - Reporting of runtime, exit code, or termination signal

Jailing features:

 - Optionally forbid use of fork or exec
 - Forbid gaining new privileges (e.g. via a setuid binary)
 - Basic seccomp filtering policies
 - Linux namespace setup (PID, mount, and network)
 - chroots, with optional read-only or shared bind-mounts to the outside

Resource limiting features:

 - rlimits for limiting single-process usage (CPU time, address space, file size)
 - cgroups for memory usage and limiting number of processes
 - Simple wall-time limiting


For a full list of features, see the configurable options with
```sh
$ ./xejail --help
```

Build
-----

`xejail` is a simple C++17 program with few dependencies. On Linux you'll need libseccomp
(e.g. `apt install libseccomp-dev`) and a relatively recent version of Clang that supports
`-std=c++17`.

To build:

```
make test
```

If you want to install it as a global executable (note that this will mark it
as setuid, so use carefully):
```
sudo make install
```


Tests
-----

Integration tests are available as a set of Python scripts in `tests/`. These
run `xejail` and observe output, so `xejail` must run as root.

You'll need `python-yaml` (for parsing the roughly-yaml-subset of the result file)
and `python-nose` (for running the tests). Then just run
```
make test
```
