CXX=clang++
CXXFLAGS=-std=c++17 -Wall -Werror -Wshadow
LDFLAGS=-lstdc++fs -lseccomp
INSTALLPATH=/usr/local/bin

OUTFILE=xejail

$(OUTFILE): src/*.cpp src/*.h
	$(CXX) $(CXXFLAGS) -o $(OUTFILE) src/*.cpp $(LDFLAGS)

clean:
	rm $(OUTFILE)

tests/test_programs/abort: tests/test_programs/abort.c
	$(CC) -o tests/test_programs/abort tests/test_programs/abort.c

tests/test_programs/segfault: tests/test_programs/segfault.c
	$(CC) -o tests/test_programs/segfault tests/test_programs/segfault.c

test: $(OUTFILE) tests/test_programs/abort tests/test_programs/segfault
	JAIL_BINARY='./xejail' python -m nose -dv

install: $(OUTFILE)
	cp $(OUTFILE) $(INSTALLPATH)
	chmod u+s $(INSTALLPATH)/$(OUTFILE)
