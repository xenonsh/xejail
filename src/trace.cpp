#include "trace.h"

#include <sys/ptrace.h>
#include <sys/signal.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

#include "result.h"

namespace xenon {
namespace jail {
namespace trace {

namespace {
void set_ptrace_options(int pid) {
  auto flags = PTRACE_O_EXITKILL | PTRACE_O_TRACEEXEC | PTRACE_O_TRACECLONE |
               PTRACE_O_TRACEFORK | PTRACE_O_TRACEVFORK | PTRACE_O_TRACESECCOMP;
  check_sys(ptrace(PTRACE_SETOPTIONS, pid, nullptr, flags),
            "failed to setup ptrace options");
}

bool is_ptrace_event(int status, int event) {
  return status >> 8 == (SIGTRAP | (event << 8));
}
}  // namespace

void ask_for_tracing() {
  ptrace(PTRACE_TRACEME, 0, nullptr, nullptr);
  check_sys(kill(getpid(), SIGSTOP), "failed to pause self");
}

void attach_child(int pid) {
  check_sys(ptrace(PTRACE_ATTACH, pid, nullptr, nullptr),
            "failed to attach with ptrace");
  int status;
  check_sys(waitpid(pid, &status, WUNTRACED),
            "failed to wait for stopped child");

  if (!WIFSTOPPED(status)) {
    throw jail_error("expected child to stop to be traced");
  }
  set_ptrace_options(pid);
  continue_child(pid);
}

void continue_child(int pid, int signal) {
  check_sys(ptrace(PTRACE_CONT, pid, 0x1, signal), "failed to resume child");
}

std::optional<int> event_from_status(int status) {
  auto events = {PTRACE_EVENT_EXEC,  PTRACE_EVENT_STOP,  PTRACE_EVENT_FORK,
                 PTRACE_EVENT_VFORK, PTRACE_EVENT_CLONE, PTRACE_EVENT_EXEC};

  for (auto event : events) {
    if (is_ptrace_event(status, event)) {
      return event;
    }
  }
  return {};
}
}  // namespace trace
}  // namespace jail
}  // namespace xenon