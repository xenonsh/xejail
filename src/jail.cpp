#include "jail.h"

#include <fcntl.h>
#include <pwd.h>
#include <sys/mount.h>
#include <sys/prctl.h>
#include <sys/ptrace.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

#include <algorithm>
#include <array>
#include <cerrno>
#include <cstring>
#include <fstream>
#include <iostream>

#include "result.h"
#include "trace.h"
#include "util.h"

namespace xenon {
namespace jail {
namespace {
void set_parent_uid(container_context& ctx) {
  ctx.parent_euid = geteuid();
  ctx.parent_egid = getegid();
}
}  // namespace

std::optional<int> open_fd_if_set(const std::string& filename,
                                  int open_flags,
                                  mode_t mode = 0) {
  if (filename.empty()) {
    return {};
  }

  int fd = open(filename.c_str(), open_flags, mode);
  check_sys(fd, "failed to open " + filename);
  return fd;
}

void get_user_ids(const std::string& user_name, int& uid, int& gid) {
  auto* pw = getpwnam(user_name.c_str());
  if (pw == nullptr) {
    throw jail_error("failed to lookup user: " + user_name);
  }
  uid = pw->pw_uid;
  gid = pw->pw_gid;
}

void jail_program::fork_monitor_for_pid_namespace(const jail_options& opts) {
  if (opts.new_user_namespace) {
    check_sys(unshare(CLONE_NEWUSER), "failed to create user namespace");
  }
  check_sys(unshare(CLONE_NEWPID), "failed to create pid namespace");
  int pid = fork();
  check_sys(pid, "failed to fork into new pid namespace");
  if (pid != 0) {
    int status;
    check_sys(waitpid(pid, &status, 0), "failed to wait for child");

    if (WIFEXITED(status)) {
      exit(status);
    } else {
      exit(EXIT_FAILURE);
    }
    std::terminate();
  }

  check_sys(prctl(PR_SET_PDEATHSIG, SIGKILL),
            "failed to set child death signal");
}

jail_program jail_program::from_opts(const jail_options& opts,
                                     const std::vector<std::string>& args) {
  jail_program prog(opts, args);
  prog.ctx.stdin_fd = open_fd_if_set(opts.stdin_file, O_RDONLY | O_CLOEXEC);

  auto create_flags = O_CREAT | O_TRUNC | O_WRONLY | O_CLOEXEC;
  auto mode = S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH;
  prog.ctx.stdout_fd = open_fd_if_set(opts.stdout_file, create_flags, mode);
  prog.ctx.stderr_fd = open_fd_if_set(opts.stderr_file, create_flags, mode);
  set_parent_uid(prog.ctx);
  get_user_ids(opts.run_as_user, prog.ctx.run_as_user_uid,
               prog.ctx.run_as_user_gid);

  if (!opts.seccomp_policy_file.empty()) {
    std::ifstream policy_file(opts.seccomp_policy_file);
    prog.ctx.seccomp_policy = seccomp_policy::load(policy_file);
  } else if (!opts.seccomp_policy_string.empty()) {
    prog.ctx.seccomp_policy = seccomp_policy::load(opts.seccomp_policy_string);
  } else if (!opts.seccomp_policy.empty()) {
    prog.ctx.seccomp_policy = seccomp_policy::get_named(opts.seccomp_policy);
    if (!prog.ctx.seccomp_policy) {
      throw jail_error("unknown builtin seccomp policy: " +
                       opts.seccomp_policy);
    }
  }
  return prog;
}

jail_result jail_program::run() {
  try {
    loop.listen_for_signal(SIGCHLD, [&](int /* signal */) {
      if (reap_child_events()) {
        loop.stop();
      }
    });
    setup_cgroups();

    int pid = fork();
    check_sys(pid, "failed to fork process");
    if (pid == 0) {
      run_child();
      // run_child() should never return
      std::terminate();
    }

    jail_pid = pid;
    cleanup_in_parent();
    monitor_child(pid);
    if (!has_done_exec && !opts.disable_tracing) {
      abort_trapped_container_or_child(pid, jail_status::SETUP_ERROR,
                                       "failed to start jail program");
    }
    result.record_stats(start_time, ctx.memory_cg);
    check_for_memory_exceeded();

    return result;
  } catch (const jail_error& exc) {
    if (jail_pid) {
      kill(jail_pid.value(), SIGKILL);
    }
    record_result(jail_status::SETUP_ERROR, std::string(exc.what()));
    return result;
  }
}

const container_context& jail_program::context() {
  return ctx;
}

void jail_program::setup_cgroups() {
  if (opts.cgroup_memory_limit_mb) {
    ctx.memory_cg.emplace(opts.cgroup_memory_mount, opts.cgroup_name);
  }
  if (opts.cgroup_pid_limit) {
    ctx.pid_cg.emplace(opts.cgroup_pid_mount, opts.cgroup_name);
  }

  if (ctx.memory_cg) {
    auto& cg = ctx.memory_cg.value();
    if (opts.cgroup_memory_limit_mb) {
      cg.set_memory_limit_mb(opts.cgroup_memory_limit_mb.value());
    }

    int eventfd = loop.create_and_listen_on_eventfd([&](int /* eventfd */) {
      abort_container(jail_status::MEMORY_LIMIT_EXCEEDED);
    });
    cg.disable_oom_killer();
    cg.disable_swap();
    cg.listen_for_oom(eventfd);
  }
  if (ctx.pid_cg) {
    auto& cg = ctx.pid_cg.value();
    if (opts.cgroup_pid_limit) {
      cg.set_pid_limit(opts.cgroup_pid_limit.value());
    }
  }
}

void jail_program::check_for_memory_exceeded() {
  // we may not trigger the OOM killer of a cgroup, but thrash
  // against the limit causing failures.
  auto potential_oom_statuses = {jail_status::WALL_TIME_EXCEEDED,
                                 jail_status::EXIT_ERROR,
                                 jail_status::EXIT_SIGNAL};
  auto is_potential_oom_status =
      util::contains(potential_oom_statuses, result.status);

  auto enough_alloc_failures = result.cgroup_memory_alloc_failures.value_or(0) >
                               opts.memory_failcnt_threshold;
  if (is_potential_oom_status && enough_alloc_failures) {
    result.abort_message = "hit memory allocation failures and got result " +
                           jail_status_to_string(result.status);
    result.status = jail_status::MEMORY_LIMIT_EXCEEDED;
  }
}

void jail_program::run_child() {
  try {
    if (!opts.disable_tracing) {
      trace::ask_for_tracing();
    }

    jail_container container(opts, ctx);
    container.setup();
    container.exec(program_args);
  } catch (const std::exception& exc) {
    std::cerr << "failed to launch jail: " << exc.what() << std::endl;
    exit(EXIT_FAILURE);
  }
}

void jail_program::record_result(jail_status status, std::string message) {
  if (status > result.status) {
    result.status = status;
    if (!message.empty()) {
      result.abort_message = std::move(message);
    }
  }
}

void jail_program::abort_container(jail_status status, std::string message) {
  if (status > result.status) {
    result.status = status;
    result.abort_message = std::move(message);
  }
  if (!jail_pid) {
    throw jail_error("trying to abort container that never started");
  }

  check_sys(kill(jail_pid.value(), SIGKILL),
            "failed to kill parent jail process");
  if (!opts.new_pid_namespace && ctx.pid_cg) {
    ctx.pid_cg.value().kill_all_grouped();
  }
}

void jail_program::abort_trapped_container_or_child(int pid,
                                                    jail_status status,
                                                    std::string message) {
  if (status > result.status) {
    result.status = status;
    result.abort_message = std::move(message);
  }
  if (!jail_pid) {
    throw jail_error("trying to abort container that never started");
  }

  trace::continue_child(pid, SIGKILL);
  if (pid != jail_pid.value()) {
    // abort the entire container if an event is hitting a child
    kill(jail_pid.value(), SIGKILL);
  }
}

void jail_program::cleanup_in_parent() {
  if (ctx.stdin_fd) {
    check_sys(close(ctx.stdin_fd.value()), "failed to close stdin file");
  }
  if (ctx.stdout_fd) {
    check_sys(close(ctx.stdout_fd.value()), "failed to close stdout file");
  }
  if (ctx.stderr_fd) {
    check_sys(close(ctx.stderr_fd.value()), "failed to close stderr file");
  }
}

void jail_program::handle_ptrace_event(pid_t pid, int ptrace_event) {
  switch (ptrace_event) {
    case PTRACE_EVENT_EXEC: {
      if (has_done_exec && !opts.allow_exec) {
        abort_trapped_container_or_child(pid, jail_status::BAD_SYSCALL,
                                         "exec() is not allowed");
      }
      has_done_exec = true;
      break;
    }
    case PTRACE_EVENT_VFORK:
    case PTRACE_EVENT_FORK: {
      if (!opts.allow_fork) {
        abort_trapped_container_or_child(pid, jail_status::BAD_SYSCALL,
                                         "fork()/vfork() is not allowed");
      }
      break;
    }
    case PTRACE_EVENT_CLONE: {
      if (!opts.allow_threads) {
        abort_trapped_container_or_child(pid, jail_status::BAD_SYSCALL,
                                         "threads are not allowed");
      }
      break;
    }
  }
  if (result.is_none()) {
    trace::continue_child(pid);
  }
}

bool jail_program::reap_child_events() {
  while (true) {
    int status = 0;
    int pid = waitpid(-1, &status, __WALL | WNOHANG);
    if (pid == 0) {
      return false;
    }

    check_sys(pid, "waitpid failed");
    if (pid == jail_pid.value()) {
      if (WIFEXITED(status)) {
        result.exit_code = WEXITSTATUS(status);
        record_result(WEXITSTATUS(status) == 0 ? jail_status::OK
                                               : jail_status::EXIT_ERROR);
        return true;
      } else if (WIFSIGNALED(status)) {
        result.exit_signal = WTERMSIG(status);
        record_result(jail_status::EXIT_SIGNAL);
        return true;
      }
    }

    if (WIFSTOPPED(status)) {
      auto signal = WSTOPSIG(status);

      switch (signal) {
        case SIGTRAP: {
          auto event = trace::event_from_status(status);
          if (!event.has_value()) {
            abort_trapped_container_or_child(
                pid, jail_status::BAD_SYSCALL,
                "jail received unexpected trace event");
            break;
          }

          handle_ptrace_event(pid, event.value());
          break;
        }
        case SIGSTOP: {
          trace::continue_child(pid);
          break;
        }
        case SIGABRT:
        case SIGSEGV:
        case SIGCHLD:
        case SIGALRM: {
          trace::continue_child(pid, signal);
          break;
        }
        case SIGXFSZ: {
          record_result(jail_status::OUTPUT_LIMIT_EXCEEDED);
          trace::continue_child(pid, SIGKILL);
          break;
        }
        case SIGXCPU: {
          record_result(jail_status::CPU_TIME_EXCEEDED);
          trace::continue_child(pid, SIGKILL);
          break;
        }
        default: {
          std::string signame(strsignal(signal));
          abort_trapped_container_or_child(
              pid, jail_status::BAD_SYSCALL,
              "jail received unexpected signal (" + signame + ")");
          break;
        }
      }
    }
  }
}

void jail_program::monitor_child(pid_t child_pid) {
  if (!opts.disable_tracing) {
    trace::attach_child(child_pid);
  }
  if (opts.wall_time_limit_ms) {
    loop.set_timeout(
        std::chrono::milliseconds(opts.wall_time_limit_ms.value()),
        [&]() { abort_container(jail_status::WALL_TIME_EXCEEDED); });
  }
  loop.run();
}
}  // namespace jail
}  // namespace xenon