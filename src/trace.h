#include <optional>

namespace xenon {
namespace jail {
namespace trace {

/* Helpers for working with ptrace() */

/*
 * Setup the current process to be ptrace()-ed. Stops the process,
 * assuming that the parent will attach to resume it.
 */
void ask_for_tracing();
/*
 * Attach to a child with ptrace(), and start listening for events.
 */
void attach_child(int pid);

/*
 * Continue a child process which is trapped due to a trace event,
 * optionally sending it a new signal.
 */
void continue_child(int pid, int signal = 0);

/*
 * Extract a ptrace event (PTRACE_EVENT_*) from a wait*() status,
 * if the status refers to a ptrace event.
 */
std::optional<int> event_from_status(int status);
}  // namespace trace
}  // namespace jail
}  // namespace xenon