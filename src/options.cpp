#include "options.h"

#include <algorithm>
#include <fstream>
#include <iostream>
#include <unordered_map>
#include <unistd.h>

#include "util.h"

#define STRING_OPT(name) parse_string(opts.name, #name, flags)
#define STRING_LIST_OPT(name) parse_string_list(opts.name, #name, flags)
#define INT_OPT(name) parse_int(opts.name, #name, flags)
#define OCTAL_INT_OPT(name) parse_octal_int(opts.name, #name, flags)
#define OPTIONAL_INT_OPT(name) parse_optional_int(opts.name, #name, flags)
#define BOOL_OPT(name) parse_bool(opts.name, #name, flags)
#define PATH_MAP_OPT(name) parse_path_map(opts.name, #name, flags)

namespace xenon {
namespace jail {

std::string options_help =
    R"HELP(xejail: wrapper around common Linux jailing options.

Usage:
  xejail [--stdin_file=/tmp/test.in ...] -- ./my_program [program_args ...]

General options:
  --profile_file=
    Absolute or relative path to a "profile" or settings file. Each line is 
    interpreted as a flag ('--' prefix omitted). This can be used to save a 
    series of options for continued runs.

Common options:
  --stdin_file=
      Absolute or relative path to a file that will be redirected 
      to the jailed program's stdin.

  --stdout_file=
      Absolute or relative path to a file that will be overwritten
      with the jailed program's stdout.

  --stderr_file=
      Absolute or relative path to a file that will be overwritten
      with the jailed program's stdout.

  --run_as_user=nobody
      User to switch to before executing the jailed program. Defaults
      to "nobody".

  --env=MY_VAR=MY_VAL
      Value added to the environment of the jailed program. May be specified
      multiple times to add multiple variables.

  --umask=002
      Octal integer value to set as the umask.

Container options:
  --new_pid_namespace=true
      Create a new PID namespace for the jailed program. The program
      will appear to have PID 1 in its own namespace.

  --new_mount_namespace=true 
      Create a new mount namespace for the jailed program. With new_pid_namespace,
      remount /proc in the new PID namespace.

  --new_network_namespace=true 
      Create a new network namespace for the jailed program. This namespace will only
      have loopback available.

  --new_uts_namespace=true
      Create a new UTS namespace. The jailed program will have a unique hostname
      only visible in the jail. See --hostname.

Chroot and workdir options:
  --chroot=
      New working root for the jailed program as a directory. If this is not populated
      with the jailed program, you will likely need --chroot_bind to get files into the
      chroot.

  --workdir=
      Working dir for the jailed program. With --chroot, this is
      the path inside the chroot. 

  --chroot_mkdir=
      Directories to create inside the chroot. These will be created with the
      effective uid of the parent program, not the jailed program.

  --chroot_bind=
      Directory to bind into the chroot. May be in the form /path/outside:/path/inside or
      just /path/outside (which will bind to the same name inside). Directory is mounted
      read only. This may be specified multiple times for multiple directories.

  --chroot_bind_shared=
      Same as --chroot_bind, but the directory will be not be remounted as readonly.

  --chroot_link=
      Hard link a single file into the chroot. Same format as --chroot_bind.

  --chroot_mount_proc=false
      If true, mount /proc inside the chroot.

  --chroot_mount_tmp=false
      If true, mount tmpfs on /tmp inside the chroot.

  --chroot_overlay_layer=
      Set of directories that form the lower (read-only) layers of an overlayfs for
      the container, to be mounted as the chroot directory. These directories will
      be merged to form the root filesystem for the chroot. Specify multiple times
      to merge many layers.

  --chroot_overlay_write_dir=
      If set along with chroot_overlay_layer, sets the upper (writable) layer
      of the overlayfs. Any new or modified fileswill be added (in sub-directories) 
      into this directory. Must set chroot_overlay_workdir to use.

  --chroot_overlay_workdir=
      Only useful with chroot_overlay_write_dir -- set as the workdir for the
      overlayfs, giving it temporary space for writes.

Resource limiting options:
  rlimit options (excepting RLIMIT_NPROC / --rlimit_num_proc) only affect
  the usage of individual processes, so may only work well with --allow_fork=false.

  --wall_time_limit_ms=
    Limit in milliseconds for the wall time of the jailed processes. The
    jail will be killed if it runs past this time limit, though it is only
    as precise as the kernel scheduler.

  --rlimit_cpu_sec=
    Limit the amount of CPU time an individual jailed process can get
    in seconds. The jail will be killed if a process reaches this limit.

  --rlimit_address_space_mb=
    Limit the size (in MB) of the address space of any individual jailed process.
    Attempts to increase the memory usage (via mmap, brk, etc) will fail with EMEM.

  --rlimit_num_procs=64
    Limit on the total number of processes and threads running for the user
    given by --run_as_user. Attempts to create threads or processes will fail
    after hitting this limit.

  --rlimit_num_fds=64
    Limit on the total number of open file descriptions for any one jailed
    process.

  --rlimit_fsize_kb=
    Maximum size (in KB) of any individual file that can be written by a jailed
    process.

  --rlimit_core_size_kb=0
    Maximum size (in KB) of core dump files a jailed process aborts with
    a dumpable signal.

  --cgroup_name=jail.$PID
    Name of the sub-cgroup created for resource limited. For example, if
    --cgroup_name=my-cgroup, a cgroup would be created at
    /sys/fs/cgroup/memory/my-cgroup/

  --cgroup_memory_mount=/sys/fs/cgroup/memory/
    Override location for the memory cgroup.

  --cgroup_pid_mount=/sys/fs/cgroup/pids/
    Override location for the PID cgroup.

  --cgroup_memory_limit_mb=
    Limit the amount of memory for all processes in the jail using cgroups.

  --cgroup_pid_limit=
    Limit the amount of processes in the jail using cgroups.
  
Syscall options:
  --allow_exec=false 
      Allow the jailed program to call execve and change the running binary.
  
  --allow_fork=false 
      Allow the jailed program to create new processes with fork, vfork, or clone.

  --allow_threads=true
      Allow the jailed program to create new threads with clone. 

Seccomp_options:
  --seccomp_policy=
      Use a predefined seccomp policy for filtering syscalls. Choose from
      "stdio_limited" (minimal configuration for programs writing to stdio
      but reading runtime files).

  --seccomp_policy_string=
      Custom seccomp policy, specified as a command format in strings.
  
  --seccomp_policy_file
      Same as --seccomp_policy_string, but load from a file.

Output options:
  --quiet=false
      If true, don't print out execution results.

  --result_file=
      Path to a file (will be created if not existing) where results will be dumped,
      in place of stdout.
)HELP";

using flag_map = std::unordered_map<std::string, std::vector<std::string>>;

flag_map parse_flag_values(const std::vector<std::string>& args) {
  flag_map flags;
  for (auto& arg : args) {
    if (arg == "--") {
      break;
    }

    auto flag = arg;
    if (flag.substr(0, 2) == "--") {
      flag = flag.substr(2);
    }

    auto split = util::split_on(flag, '=');
    if (split) {
      auto& [flag_name, flag_value] = split.value();
      flags[flag_name].push_back(flag_value);
    } else {
      flags[flag].push_back("");
    }
  }
  return flags;
}

template <typename fn>
void parse_single_value(fn&& func, const std::string& name, flag_map& flags) {
  auto it = flags.find(name);
  if (it != flags.end()) {
    auto& values = it->second;

    if (values.size() != 1) {
      throw option_parse_error("expected only one value for flag " + name);
    }
    auto& value = values.front();
    func(value);
    flags.erase(it);
  }
}

int int_from_string(const std::string& value, size_t base = 10) {
  auto val = util::to_int(value, base);
  if (!val) {
    throw option_parse_error("invalid int value: " + value);
  }
  return val.value();
}

void parse_int(int& opt, const std::string& name, flag_map& flags) {
  parse_single_value([&](auto& value) { opt = int_from_string(value); }, name,
                     flags);
}

void parse_octal_int(int& opt, const std::string& name, flag_map& flags) {
  parse_single_value(
      [&](auto& value) { opt = int_from_string(value, /* base */ 8); }, name,
      flags);
}

void parse_optional_int(std::optional<int>& opt,
                        const std::string& name,
                        flag_map& flags) {
  parse_single_value(
      [&](auto& value) {
        if (value == "none" || value == "") {
          opt.reset();
        } else {
          opt = int_from_string(value);
        }
      },
      name, flags);
}

void parse_bool(bool& opt, const std::string& name, flag_map& flags) {
  parse_single_value(
      [&](auto& value) {
        if (value == "" || value[0] == 't' || value[0] == 'y') {
          opt = true;
        } else if (value[0] == 'f' || value[0] == 'n') {
          opt = false;
        } else {
          throw option_parse_error(
              "failed to parse flags, invalid value for bool flag " + name +
              ": " + value);
        }
      },
      name, flags);
}

void parse_string(std::string& opt, const std::string& name, flag_map& flags) {
  parse_single_value([&](auto& value) { opt = value; }, name, flags);
}

void parse_string_list(std::vector<std::string>& opt,
                       const std::string& name,
                       flag_map& flags) {
  auto it = flags.find(name);
  if (it != flags.end()) {
    opt = it->second;
    flags.erase(it);
  }
}

void parse_path_map(std::vector<std::pair<std::string, std::string>>& opt,
                    const std::string& name,
                    flag_map& flags) {
  auto it = flags.find(name);
  if (it != flags.end()) {
    auto& values = it->second;
    for (auto& value : values) {
      auto split = util::split_on(value, ':');
      if (split) {
        opt.push_back(split.value());
      } else {
        opt.push_back(std::make_pair(value, value));
      }
    }
    flags.erase(it);
  }
}

flag_map read_flags_from_file(const std::string& filename) {
  std::ifstream profile_file(filename);

  std::vector<std::string> args;
  while (!profile_file.eof()) {
    std::string line;
    std::getline(profile_file, line);

    if (line.empty()) {
      continue;
    }
    if (line == "--") {
      throw option_parse_error("unexpected -- in profile file");
    }
    args.push_back(line);
  }
  return parse_flag_values(args);
}

void parse_from_flags(jail_options& opts, flag_map& flags) {
  STRING_OPT(profile_file);
  if (!opts.profile_file.empty()) {
    auto file_flags = read_flags_from_file(opts.profile_file);

    jail_options base_opts;
    parse_from_flags(base_opts, file_flags);

    opts = base_opts;
  }

  BOOL_OPT(new_pid_namespace);
  BOOL_OPT(new_mount_namespace);
  BOOL_OPT(new_network_namespace);
  BOOL_OPT(new_uts_namespace);
  BOOL_OPT(new_user_namespace);

  STRING_OPT(chroot);
  STRING_OPT(workdir);
  STRING_LIST_OPT(chroot_mkdir);
  PATH_MAP_OPT(chroot_bind);
  PATH_MAP_OPT(chroot_bind_shared);
  PATH_MAP_OPT(chroot_link);
  BOOL_OPT(chroot_mount_proc);
  BOOL_OPT(chroot_mount_tmp);
  STRING_LIST_OPT(chroot_overlay_layer);
  STRING_OPT(chroot_overlay_workdir);
  STRING_OPT(chroot_overlay_write_dir);

  OPTIONAL_INT_OPT(wall_time_limit_ms);

  OPTIONAL_INT_OPT(rlimit_cpu_sec);
  OPTIONAL_INT_OPT(rlimit_address_space_mb);
  OPTIONAL_INT_OPT(rlimit_num_procs);
  OPTIONAL_INT_OPT(rlimit_num_fds);
  OPTIONAL_INT_OPT(rlimit_fsize_kb);
  OPTIONAL_INT_OPT(rlimit_core_size_kb);

  STRING_OPT(cgroup_name);
  STRING_OPT(cgroup_memory_mount);
  STRING_OPT(cgroup_pid_mount);
  OPTIONAL_INT_OPT(cgroup_memory_limit_mb);
  OPTIONAL_INT_OPT(cgroup_pid_limit);

  STRING_OPT(seccomp_policy);
  STRING_OPT(seccomp_policy_string);
  STRING_OPT(seccomp_policy_file);

  BOOL_OPT(allow_exec);
  BOOL_OPT(allow_fork);
  BOOL_OPT(allow_threads);

  STRING_OPT(stdin_file);
  STRING_OPT(stdout_file);
  STRING_OPT(stderr_file);
  STRING_OPT(run_as_user);

  OCTAL_INT_OPT(umask);

  STRING_LIST_OPT(env);

  BOOL_OPT(quiet);
  STRING_OPT(result_file);

  BOOL_OPT(forbid_new_privs);
  BOOL_OPT(disable_tracing);
  BOOL_OPT(drop_user_pre_fork);
  STRING_OPT(hostname);
  INT_OPT(memory_failcnt_threshold);

  if (!flags.empty()) {
    auto first = flags.begin()->first;
    throw option_parse_error("unrecognized option: " + first);
  }
}

jail_options parse_options(const std::vector<std::string>& args) {
  auto flags = parse_flag_values(args);
  jail_options opts;
  parse_from_flags(opts, flags);
  if (opts.cgroup_name.empty()) {
    opts.cgroup_name = "jail." + std::to_string(getpid());
  }
  return opts;
}

std::vector<std::string> get_program_args(
    const std::vector<std::string>& args) {
  std::vector<std::string> prog_args;

  auto it = std::find(args.begin(), args.end(), "--");
  if (it == args.end()) {
    return {};
  }
  std::copy(it + 1, args.end(), std::back_inserter(prog_args));
  return prog_args;
}
}  // namespace jail
}  // namespace xenon