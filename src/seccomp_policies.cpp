#include "seccomp.h"

#define CHECK_POLICY(policy) \
  if (name == #policy) {     \
    return load(policy);     \
  }

namespace xenon {
namespace jail {
namespace {
/*
 * Locked down seccomp policy, aimed at programs that only really
 * need to do I/O on stdio and may need to read arbitrary files for runtime.
 * Explicitly forbids opening files with write privileges, cloning (except
 * for threads), and network-related syscalls.
 */
std::string stdio_limited = R"POLICY(
default die

if [ $1 < 3 ] error EPERM close
allow close

if [ $2 & 3 == 0 ] allow open

if [ $3 & 3 == 0 ] allow openat

if [ $1 & 65536 == 0 ] allow clone

if [ $1 > 2 ] error EPERM write

allow read
allow write
allow lseek

allow fstat
allow lstat
allow stat
allow statfs
allow readlink

allow getdents
allow getcwd

allow getpid
allow getuid
allow getgid
allow geteuid
allow getegid

allow sysinfo

allow brk
allow mmap
allow munmap
allow mprotect
allow arch_prctl

allow set_tid_address
allow set_robust_list
allow rt_sigaction
allow rt_sigprocmask
allow prlimit64

allow access

allow ioctl

allow futex

allow execve

allow exit
allow exit_group
allow rt_sigreturn

error EPERM socket
error EPERM connect
)POLICY";
}  // namespace

std::optional<seccomp_policy> seccomp_policy::get_named(
    const std::string& name) {
  CHECK_POLICY(stdio_limited);

  return {};
}
}  // namespace jail
}  // namespace xenon