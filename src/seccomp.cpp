#include "seccomp.h"

#include <seccomp.h>

#include <unordered_map>

#include "result.h"
#include "util.h"

#define ADD_SYSCALL(name) \
  syscalls_by_name.insert(std::make_pair(#name, SCMP_SYS(name)))
#define ADD_ERRNO(name) errno_by_name.insert(std::make_pair(#name, name))

namespace xenon {
namespace jail {

namespace {
std::unordered_map<std::string, int> syscalls_by_name;
std::unordered_map<std::string, int> errno_by_name;

void build_name_indices() {
  if (syscalls_by_name.empty()) {
#include "syscalls.h"
  }
  if (errno_by_name.empty()) {
#include "errnos.h"
  }
}

template <typename validation_fn>
auto take(std::istream& stream, validation_fn&& fn) {
  std::string token;
  stream >> token;
  return fn(token);
}

std::string take_one_of(std::istream& stream,
                        std::vector<std::string> choices) {
  return take(stream, [&](auto& token) {
    auto it = std::find(choices.begin(), choices.end(), token);
    if (it == choices.end()) {
      throw policy_parse_error(
          stream, "saw " + token +
                      " but expected one of: " + util::join_on(choices, ","));
    }
    return token;
  });
}

uint16_t take_errno(std::istream& stream) {
  return take(stream, [&](auto& token) {
    auto it = errno_by_name.find(token);
    if (it == errno_by_name.end()) {
      throw policy_parse_error(stream, "unexpected errno value: " + token);
    }
    return it->second;
  });
}

uint32_t take_action(std::istream& stream, const std::string& cmd) {
  if (cmd == "allow") {
    return SCMP_ACT_ALLOW;
  } else if (cmd == "die") {
    return SCMP_ACT_KILL;
  } else if (cmd == "error") {
    return SCMP_ACT_ERRNO(take_errno(stream));
  } else if (cmd == "log") {
    return SCMP_ACT_LOG;
  } else {
    throw policy_parse_error(stream, "unexpected command: " + cmd);
  }
}

int take_syscall(std::istream& stream) {
  return take(stream, [&](auto& token) {
    auto it = syscalls_by_name.find(token);
    if (it == syscalls_by_name.end()) {
      throw policy_parse_error(stream, "unknown syscall: " + token);
    }
    return it->second;
  });
}

int take_int(std::istream& stream) {
  return take(stream, [&](auto& token) {
    auto rv = util::to_int(token);
    if (!rv) {
      throw policy_parse_error(stream, "bad int value: " + token);
    }
    return rv.value();
  });
}

scmp_arg_cmp take_arg_cmp(std::istream& stream, const std::string& arg) {
  scmp_arg_cmp cmp;
  cmp.arg = util::to_int(arg.substr(1)).value() - 1;

  auto opstring = take_one_of(stream, {"==", "!=", "&", ">", "<"});
  if (opstring == "==") {
    cmp.op = SCMP_CMP_EQ;
    cmp.datum_a = take_int(stream);
  } else if (opstring == "!=") {
    cmp.op = SCMP_CMP_NE;
    cmp.datum_a = take_int(stream);
  } else if (opstring == ">") {
    cmp.op = SCMP_CMP_GT;
    cmp.datum_a = take_int(stream);
  } else if (opstring == "<") {
    cmp.op = SCMP_CMP_LT;
    cmp.datum_a = take_int(stream);
  } else if (opstring == "&") {
    cmp.op = SCMP_CMP_MASKED_EQ;
    cmp.datum_a = take_int(stream);
    take_one_of(stream, {"=="});
    cmp.datum_b = take_int(stream);
  } else {
    throw policy_parse_error(stream, "bad comparison op: " + opstring);
  }
  return cmp;
}

std::vector<std::string> action_commands = {"allow", "die", "error", "log"};

void take_one(seccomp_policy& policy, std::istream& stream) {
  auto cmd = take_one_of(stream,
                         {"default", "if", "allow", "die", "error", "log", ""});
  if (cmd == "") {
    return;
  }

  auto is_action_cmd = util::contains(action_commands, cmd);
  if (cmd == "default") {
    policy.default_action =
        take_action(stream, take_one_of(stream, action_commands));
  } else if (cmd == "if") {
    take_one_of(stream, {"["});
    seccomp_rule rule;
    while (true) {
      auto arg = take_one_of(stream, {"]", "$1", "$2", "$3", "$4", "$5"});
      if (arg == "]") {
        break;
      }
      rule.arg_cmps.push_back(take_arg_cmp(stream, arg));
    }
    cmd = take_one_of(stream, action_commands);
    rule.action = take_action(stream, cmd);
    rule.syscall = take_syscall(stream);
    policy.rules.push_back(rule);
  } else if (is_action_cmd) {
    seccomp_rule rule;
    rule.action = take_action(stream, cmd);
    rule.syscall = take_syscall(stream);
    policy.rules.push_back(rule);
  }
}
}  // namespace

seccomp_policy seccomp_policy::load(std::istream& stream) {
  build_name_indices();

  seccomp_policy policy;
  while (!stream.eof()) {
    take_one(policy, stream);
  }
  return policy;
}

seccomp_policy seccomp_policy::load(const std::string& value) {
  std::istringstream stream(value);
  return load(stream);
}

void seccomp_policy::apply() {
  auto ctx = seccomp_init(default_action);
  try {
    for (auto& rule : rules) {
      int rc =
          seccomp_rule_add_array(ctx, rule.action, rule.syscall,
                                 rule.arg_cmps.size(), rule.arg_cmps.data());
      check_sys(rc,
                "failed to load seccomp rule: " + std::to_string(rule.syscall));
    }
    check_sys(seccomp_load(ctx), "failed to load seccomp filter");
  } catch (...) {
    seccomp_release(ctx);
    throw;
  }
}

}  // namespace jail
}  // namespace xenon