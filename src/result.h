#pragma once

#include <chrono>
#include <iostream>
#include <optional>

#include "options.h"

namespace xenon {
namespace jail {

/*
 * Status (or really, an error code) enum for finished jailed programs.
 */
enum jail_status : int {
  // Should not be set for a completed program
  NONE = 0,

  // Normal termination (from the view of the jailer)
  OK = 1,
  EXIT_ERROR = 2,
  EXIT_SIGNAL = 3,

  // Resource limits exceeded and process was killed or crashed
  CPU_TIME_EXCEEDED = 4,
  WALL_TIME_EXCEEDED = 5,
  MEMORY_LIMIT_EXCEEDED = 6,
  OUTPUT_LIMIT_EXCEEDED = 7,

  // Jailed program attempted to do something that was not allowed,
  // commonly invoking a disallowed syscall (e.g. fork())
  BAD_SYSCALL = 8,
  // Internal error, jailed program never started
  SETUP_ERROR = 9,
};

/*
 * Format the jail status as a string, same as the enum name but lower case.
 */
std::string jail_status_to_string(jail_status status);

class memory_cgroup;

struct jail_result {
  jail_status status{jail_status::NONE};

  std::optional<int> exit_code;
  std::optional<int> exit_signal;
  std::optional<std::string> abort_message;

  std::chrono::milliseconds wall_time{0};
  std::chrono::milliseconds usr_cpu_time{0};
  std::chrono::milliseconds sys_cpu_time{0};
  std::chrono::milliseconds cpu_time{0};

  int max_rss_used_mb{0};
  std::optional<int> cgroup_memory_usage_mb;
  std::optional<int> cgroup_memory_alloc_failures;

  bool is_none() const { return status == jail_status::NONE; }

  /*
   * Record resource usage stats after the jailed program is finished,
   * by inspecting cgroups and other system resources (e.g. getrusage()).
   */
  void record_stats(std::chrono::steady_clock::time_point start_time,
                    std::optional<memory_cgroup>& mem_cg);
  /*
   * Dump status and resource usage stats to the given stream.
   *
   * Format is similar to YAML, though it is manually output. For example,
   * ```
   * status: ok
   * exit_code: 0
   * wall_time_ms: 39
   * cpu_time_ms: 2
   * usr_cpu_time_ms: 0
   * sys_cpu_time_ms: 2
   * max_rss_used_mb: 2
   * ```
   */
  void dump(std::ostream& stream);
};

/*
 * Typed exception wrapper for any error thrown during jail setup
 * or execution.
 */
class jail_error : public std::exception {
 public:
  explicit jail_error(std::string msg) : message(std::move(msg)) {}

  const char* what() const noexcept override { return message.c_str(); }

 private:
  std::string message;
};

/*
 * Error checking wrapper for syscalls (or other C-style functions)
 * that converts an error return value (an integer where < 0 means error)
 * into an exception of type jail_error if an error occurred.
 *
 * Prefer using this over C-style error handling as it makes it harder to
 * forget to handle errors.
 */
void check_sys(int retn, const std::string& message);

}  // namespace jail
}  // namespace xenon