#include "cgroup.h"

#include <fcntl.h>
#include <signal.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#include <fstream>
#include <iostream>

#include "result.h"

namespace xenon {
namespace jail {
cgroup::cgroup(util::fs::path mount_point, std::string name)
    : cg_path(mount_point / name) {
  util::fs::create_directory(cg_path);
}
cgroup::~cgroup() {
  if (!cg_path.empty()) {
    util::fs::remove(cg_path);
  }
}

void cgroup::detach() {
  cg_path = "";
}

void cgroup::insert_task(int pid) {
  std::ofstream outf(cg_path / "tasks");
  outf << pid << std::endl;
}

void cgroup::kill_all_grouped() {
  std::vector<int> pids;
  std::ifstream tasks_file(cg_path / "tasks");

  while (!tasks_file.eof()) {
    int pid;
    tasks_file >> pid;
    pids.push_back(pid);
  }

  for (auto pid : pids) {
    kill(pid, SIGKILL);
  }
}

memory_cgroup::memory_cgroup(util::fs::path mount_point, std::string name)
    : cgroup(mount_point, name) {}

void memory_cgroup::set_memory_limit_mb(int limit_in_mb) {
  std::ofstream limit_in_bytes_file(cg_path / "memory.limit_in_bytes");
  limit_in_bytes_file << limit_in_mb << "M" << std::endl;
}

void memory_cgroup::disable_oom_killer() {
  std::ofstream oom_control_file(cg_path / "memory.oom_control");
  oom_control_file << 1 << std::endl;
}

void memory_cgroup::disable_swap() {
  std::ofstream swappiness_file(cg_path / "memory.swappiness;");
  swappiness_file << 0 << std::endl;
}

void memory_cgroup::listen_for_oom(int eventfd) {
  auto path = cg_path / "memory.oom_control";
  int oom_control_fd = open(path.c_str(), O_RDONLY | O_CLOEXEC);
  check_sys(oom_control_fd, "failed to open memory.oom_control");

  std::ofstream event_control_file(cg_path / "cgroup.event_control");
  event_control_file << eventfd << " " << oom_control_fd << std::endl;

  close(oom_control_fd);
}

long memory_cgroup::get_max_usage_mb() {
  std::ifstream max_usage_file(cg_path / "memory.max_usage_in_bytes");
  long usage;
  max_usage_file >> usage;
  return usage / 1024 / 1024;
}

long memory_cgroup::get_fail_count() {
  std::ifstream failcnt_file(cg_path / "memory.failcnt");
  long count;
  failcnt_file >> count;
  return count;
}

pid_cgroup::pid_cgroup(util::fs::path mount_point, std::string name)
    : cgroup(mount_point, name) {}

void pid_cgroup::set_pid_limit(int pids) {
  std::ofstream pids_max_file(cg_path / "pids.max");
  pids_max_file << pids << std::endl;
}

}  // namespace jail
}  // namespace xenon
