#include <cassert>
#include <chrono>
#include <functional>
#include <unordered_map>

namespace xenon {
namespace jail {

/*
 * Primitive async event-loop based on epoll and a set of file descriptors.
 * Effectively, this lets the supervisor process monitor a small set of things
 * concurrently without using threads (which are a nightmare in multi-process
 * scenarios).
 *
 * Supports three file descriptor types:
 *  - signalfd, primarily for handling SIGCHLD instead of blocking on
 *    waitpid() in a loop.
 *  - eventfd, for listening on the memory cgroup's OOM-control (see cgroup.h).
 *  - timerfd, for limiting wall time of the jailed process.
 */
class event_loop {
 private:
  enum event_loop_type {
    UNKNOWN = 0,
    SIGNALFD = 1,
    EVENTFD = 2,
    TIMERFD = 3,
  };

 public:
  event_loop();
  ~event_loop();

  using signal_callback = std::function<void(int signal)>;
  using eventfd_callback = std::function<void(int eventfd)>;
  using timer_callback = std::function<void()>;

  /*
   * Start the event loop, blocking the current thread and dispatching
   * callbacks until stop() is called.
   */
  void run();
  /*
   * Stop the event loop from running the next iteration.
   */
  void stop();

  /*
   * Set a callback that be triggered when the current process is sent
   * the given signal by polling on a signalfd. Note that this requires
   * blocking normal signal delivery, which should be unblocked in
   * children.
   */
  void listen_for_signal(int signal, signal_callback callback);
  /*
   * Create a return a new eventfd. Callback will be called when the
   * eventfd has been notified.
   */
  int create_and_listen_on_eventfd(eventfd_callback callback);
  /*
   * Set a timeout relative to the current time using a timerfd. Callback
   * will be called (roughly) after the specified timeout, assuming the
   * eventloop is still running.
   */
  void set_timeout(std::chrono::milliseconds timeout, timer_callback callback);

 private:
  void add_fd(int fd);
  template <typename callback_fn, typename map_type>
  void add_fd_and_callback(int fd,
                           event_loop_type type,
                           callback_fn&& callback,
                           map_type& map) {
    add_fd(fd);
    {
      auto inserted_pair = fd_types.insert(std::make_pair(fd, type));
      assert(inserted_pair.second);
    }
    {
      auto inserted_pair = map.insert(
          std::make_pair(std::move(fd), std::forward<callback_fn>(callback)));
      assert(inserted_pair.second);
    }
  }

  void close_and_remove_fd(int fd);

 private:
  bool running{false};
  int epoll_fd{-1};

  template <typename T>
  using fd_map = std::unordered_map<int, T>;

  fd_map<event_loop_type> fd_types;
  fd_map<signal_callback> signalfd_callbacks;
  fd_map<eventfd_callback> eventfd_callbacks;
  fd_map<timer_callback> timerfd_callbacks;
};
}  // namespace jail
}  // namespace xenon