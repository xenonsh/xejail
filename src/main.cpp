#include <sys/wait.h>
#include <unistd.h>

#include <fstream>
#include <iostream>

#include "jail.h"
#include "options.h"
#include "util.h"

using namespace xenon::jail;

int main(int argc, char* argv[]) {
  // file descriptors are inherited by all children, so we must be careful not
  // to leak anything to the jailed program. Particularly bad with chroots,
  // because a leaked file descriptor may point outside of the chroot.
  // we are careful to mark all opened fds from the parent with O_CLOEXEC
  // to prevent this problem, but some file descriptors may be leaked from
  // parents of xejail.
  util::try_close_all_other_fds({STDIN_FILENO, STDOUT_FILENO, STDERR_FILENO});

  std::vector<std::string> args;
  for (int i = 1; i < argc; ++i) {
    args.push_back(std::string(argv[i]));
  }

  if (args.size() == 0 || args[0] == "-h" || args[0] == "--help" ||
      args[0] == "help") {
    std::cout << options_help;
    return 0;
  }

  try {
    auto opts = parse_options(args);
    auto program_args = get_program_args(args);
    if (program_args.size() == 0) {
      std::cerr << options_help;
      return 1;
    }

    auto program = jail_program::from_opts(opts, program_args);
    if (opts.new_pid_namespace) {
      // Fun note about Linux, PID namespaces, signals, and ptrace:
      // The first process created inside a new PID namespace gets PID 1
      // in the new namespace (much like init outside any namespaces) and
      // has many properties of init -- if it dies the whole namespace dies,
      // it takes any orphan processes, etc. init also has the property that
      // the kernel _does not deliver signals that init does not have an
      // explicit handler for_. This means you cannot send init SIGTERM,
      // SIGSEGV, etc unless it set up a handler.
      //
      // This is reasonable but when we start to ptrace() the child process,
      // the child traps whenever it gets a signal (including regular things
      // like aborting or segfaulting) and we manually redeliver it with
      // PTRACE_CONT. Unfortunately, the above caveat applies with respect
      // to init taking signals if the process is in the new PID namespace.
      // So if we are ptrace()-ing from outside the namespace and the "init"
      // segfaults, there's no way to deliver that signal to the process and
      // it is trapped in an infinite loop of trying to crash.
      //
      // Hence, we fork here, and make ourselves PID 1 and monitor from inside
      // the namespace so we can handle signals properly.
      jail_program::fork_monitor_for_pid_namespace(opts);

      if (opts.drop_user_pre_fork && opts.new_user_namespace &&
          (geteuid() == 0 || getegid() == 0)) {
        switch_users(opts, program.context());
      }
    }

    auto result = program.run();
    if (!opts.quiet) {
      if (opts.result_file.empty()) {
        result.dump(std::cout);
      } else {
        std::ofstream out_file(opts.result_file,
                               std::ios::trunc | std::ios::out);
        result.dump(out_file);
      }
    }

    if (result.status != jail_status::OK) {
      return 1;
    }
    return 0;
  } catch (const std::exception& exc) {
    std::cerr << exc.what() << std::endl;
    return 1;
  }
  return 0;
}