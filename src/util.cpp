#include "util.h"

#include <unistd.h>

#include <experimental/filesystem>
#include <iostream>

namespace xenon {
namespace jail {
namespace util {

namespace fs = std::experimental::filesystem;

std::optional<int> to_int(const std::string& value, size_t base) {
  try {
    size_t pos;
    auto rv = std::stoi(value, &pos, base);
    if (pos != value.size()) {
      return {};
    }
    return rv;
  } catch (std::invalid_argument& exc) {
    return {};
  }
}

std::optional<std::pair<std::string, std::string>> split_on(
    const std::string& value,
    char delim) {
  auto pos = value.find(delim);
  if (pos == std::string::npos) {
    return {};
  }
  return std::make_pair(value.substr(0, pos), value.substr(pos + 1));
}

std::string join_on(const std::vector<std::string>& strings,
                    std::string delim) {
  std::string out;
  for (auto it = strings.begin(); it != strings.end(); ++it) {
    out += *it;
    if (it + 1 != strings.end()) {
      out += delim;
    }
  }
  return out;
}

void try_close_all_other_fds(const std::unordered_set<int>& valid_fds) {
  fs::path proc_fd_path = "/proc/self/fd/";

  std::unordered_set<int> open_fds;
  if (fs::is_directory(proc_fd_path)) {
    for (auto& path : fs::directory_iterator(proc_fd_path)) {
      auto filename = path.path().filename();
      auto fd_value = to_int(filename);
      if (fd_value) {
        open_fds.insert(fd_value.value());
      }
    }
  }
  for (auto& fd : open_fds) {
    if (!valid_fds.count(fd)) {
      close(fd);
    }
  }
}

fs::path path_join(const fs::path& lhs, const fs::path& rhs) {
  return lhs / rhs;
}
}  // namespace util
}  // namespace jail
}  // namespace xenon