#pragma once

#include <optional>
#include <string>
#include <vector>

namespace xenon {
namespace jail {

/*
 * Configuration options for running the jailed program, along with a
 * primitive CLI parser for said options.
 *
 * The goal is to allow easy delegation of system operations to xejail
 * from a higher-level program. As such, the argument format is designed
 * for passing in a list of well-known names and not so much for human
 * use.
 *
 * Generally, arguments are specified like so:
 *  $ ./xejail --allow_fork=true --allow_exec=true --chroot=/tmp/chroot -- ls
 * /tmp
 *
 * The final `--` preceding the program-to-run are mandatory: all arguments
 * before * the first -- are interpreted as options and all arguments after
 * are passed directly to the run program.
 *
 * For option flags, prefixing with `--` is unnecessary. That is,
 *  $ ./xejail --allow_fork=true
 * and
 *  $ ./xejail allow_fork=true
 * are equivalent.
 *
 * For boolean-type flags, the =true may be omitted to specify a true value,
 * or you can use single-character abbreviations:
 *   - `allow_fork`, `allow_fork=t`, and `allow_fork=true` are all the same
 *   - `allow_fork=f` is the same as `--allow_fork=false`
 *
 */
extern std::string options_help;

/*
 * Typed exception wrapper (to allow explicit catches) for parsing-related
 * errors when parsing command line arguments, or options from an arguments
 * file.
 */
struct option_parse_error : public std::exception {
 public:
  explicit option_parse_error(std::string msg) : message(std::move(msg)) {}

  const char* what() const noexcept override { return message.c_str(); }

 private:
  std::string message;
};

/*
 * A parsed set of options for jailing a program.
 *
 * Default values can be set here. New options are added here, then
 * parsed in ::parse_options(). Documentation is added manually to
 * ::options_help.
 */
struct jail_options {
  std::string profile_file;

  bool new_pid_namespace{true};
  bool new_mount_namespace{true};
  bool new_network_namespace{true};
  bool new_uts_namespace{true};
  bool new_user_namespace{false};

  std::string chroot;
  std::string workdir;
  std::vector<std::string> chroot_mkdir;
  std::vector<std::pair<std::string, std::string>> chroot_link;
  std::vector<std::pair<std::string, std::string>> chroot_bind;
  std::vector<std::pair<std::string, std::string>> chroot_bind_shared;
  bool chroot_mount_proc{false};
  bool chroot_mount_tmp{false};
  std::vector<std::string> chroot_overlay_layer;
  std::string chroot_overlay_write_dir;
  std::string chroot_overlay_workdir;

  std::vector<std::string> env;

  std::optional<int> wall_time_limit_ms;

  std::optional<int> rlimit_cpu_sec;
  std::optional<int> rlimit_address_space_mb;
  std::optional<int> rlimit_num_procs{64};
  std::optional<int> rlimit_num_fds{64};
  std::optional<int> rlimit_fsize_kb{128};
  std::optional<int> rlimit_core_size_kb{0};

  std::string cgroup_name;
  std::string cgroup_memory_mount{"/sys/fs/cgroup/memory"};
  std::string cgroup_pid_mount{"/sys/fs/cgroup/pids"};
  std::optional<int> cgroup_memory_limit_mb;
  std::optional<int> cgroup_pid_limit;

  std::string seccomp_policy;
  std::string seccomp_policy_string;
  std::string seccomp_policy_file;

  std::string stdin_file;
  std::string stdout_file;
  std::string stderr_file;

  std::string run_as_user{"nobody"};

  int umask{0002};

  bool allow_exec{false};
  bool allow_fork{false};
  bool allow_threads{true};

  bool quiet{false};
  std::string result_file;

  // Non-documented options, unexpected to be changed frequently
  bool forbid_new_privs{true};
  bool disable_tracing{false};
  bool drop_user_pre_fork{true};
  std::string hostname{"jail"};
  int memory_failcnt_threshold{100};
};

/*
 * Parse a list of command-line arguments into a set of jail options.
 *
 * `args` is not expected to contain a typical $argv[0] (program name),
 * and should start with the first flag, or `--` if no flags are specified.
 *
 * Will throw option_parse_error if a flag cannot be parsed or an invalid
 * flag is specified.
 */
jail_options parse_options(const std::vector<std::string>& args);

/*
 * Get the to-run-program and its arguments. Effectively this is all args
 * after the first "--" value in the list.
 */
std::vector<std::string> get_program_args(const std::vector<std::string>& args);

}  // namespace jail
}  // namespace xenon