#include "eventloop.h"

#include <signal.h>
#include <sys/epoll.h>
#include <sys/eventfd.h>
#include <sys/signalfd.h>
#include <sys/timerfd.h>
#include <unistd.h>

#include <cassert>

#include "result.h"

namespace xenon {
namespace jail {

constexpr int MAX_EVENTS = 64;

event_loop::event_loop() {
  epoll_fd = epoll_create1(EPOLL_CLOEXEC);
  check_sys(epoll_fd, "failed to create epoll fd");
}
event_loop::~event_loop() {
  std::vector<int> fds_to_close;
  for (auto& fd_type : fd_types) {
    fds_to_close.push_back(fd_type.first);
  }

  for (auto fd : fds_to_close) {
    close_and_remove_fd(fd);
  }

  assert(fd_types.empty());
  assert(timerfd_callbacks.empty());
  assert(eventfd_callbacks.empty());
  assert(signalfd_callbacks.empty());

  close(epoll_fd);
}

void event_loop::run() {
  running = true;

  while (running) {
    std::array<epoll_event, MAX_EVENTS> events;
    int num = epoll_wait(epoll_fd, &events[0], MAX_EVENTS, -1);
    check_sys(num, "failed to call epoll_wait");

    for (int i = 0; i < num; ++i) {
      auto& event = events[i];

      auto type = fd_types.at(event.data.fd);
      switch (type) {
        case event_loop_type::TIMERFD: {
          auto& callback = timerfd_callbacks.at(event.data.fd);
          callback();
          close_and_remove_fd(event.data.fd);
          break;
        }
        case event_loop_type::SIGNALFD: {
          signalfd_siginfo info;
          auto bytes_read = read(event.data.fd, &info, sizeof(info));
          check_sys(bytes_read, "failed to read from signalfd");
          assert(bytes_read == sizeof(info));

          auto& callback = signalfd_callbacks.at(event.data.fd);
          callback(info.ssi_signo);
          break;
        }
        case event_loop_type::EVENTFD: {
          uint64_t value;
          auto bytes_read = read(event.data.fd, &value, sizeof(value));
          check_sys(bytes_read, "failed to read from eventfd");
          assert(bytes_read == sizeof(value));
          auto& callback = eventfd_callbacks.at(event.data.fd);
          callback(event.data.fd);
          break;
        }
        default: {
          break;
        }
      }
    }
  }
}
void event_loop::stop() {
  running = false;
}

void event_loop::listen_for_signal(int signal, signal_callback callback) {
  sigset_t signal_set;
  sigemptyset(&signal_set);
  sigaddset(&signal_set, signal);

  check_sys(sigprocmask(SIG_BLOCK, &signal_set, nullptr),
            "failed to block signal");

  int fd = signalfd(-1, &signal_set, SFD_CLOEXEC);
  check_sys(fd, "failed to create signalfd");

  add_fd_and_callback(fd, event_loop_type::SIGNALFD, std::move(callback),
                      signalfd_callbacks);
}

int event_loop::create_and_listen_on_eventfd(eventfd_callback callback) {
  int fd = eventfd(0, EFD_CLOEXEC);
  check_sys(fd, "failed to create eventfd");

  add_fd_and_callback(fd, event_loop_type::EVENTFD, std::move(callback),
                      eventfd_callbacks);
  return fd;
}

void event_loop::set_timeout(std::chrono::milliseconds timeout,
                             timer_callback callback) {
  int fd = timerfd_create(CLOCK_MONOTONIC, TFD_CLOEXEC);
  check_sys(fd, "failed to create timerfd");

  itimerspec timeout_value;
  timeout_value.it_interval.tv_sec = 0;
  timeout_value.it_interval.tv_nsec = 0;

  timeout_value.it_value.tv_sec = timeout.count() / 1000;
  timeout_value.it_value.tv_nsec = (timeout.count() % 1000) * 1000 * 1000;
  check_sys(timerfd_settime(fd, 0, &timeout_value, nullptr),
            "failed to setup timerfd");

  add_fd_and_callback(fd, event_loop_type::TIMERFD, std::move(callback),
                      timerfd_callbacks);
}

void event_loop::add_fd(int fd) {
  epoll_event event;
  event.events = EPOLLIN;
  event.data.fd = fd;
  check_sys(epoll_ctl(epoll_fd, EPOLL_CTL_ADD, fd, &event),
            "failed to add fd to epoll");
}

void event_loop::close_and_remove_fd(int fd) {
  auto it = fd_types.find(fd);
  assert(it != fd_types.end());
  auto fd_type = it->second;
  fd_types.erase(it);

  switch (fd_type) {
    case event_loop_type::SIGNALFD:
      signalfd_callbacks.erase(fd);
      break;
    case event_loop_type::EVENTFD:
      eventfd_callbacks.erase(fd);
      break;
    case event_loop_type::TIMERFD:
      timerfd_callbacks.erase(fd);
      break;
    default:
      std::terminate();
  }

  check_sys(epoll_ctl(epoll_fd, EPOLL_CTL_DEL, fd, nullptr),
            "failed to remove epoll fd");
  check_sys(close(fd), "failed to close event loop fd");
}
}  // namespace jail
}  // namespace xenon