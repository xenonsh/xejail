#include "container.h"

#include <grp.h>
#include <signal.h>
#include <sys/mount.h>
#include <sys/prctl.h>
#include <sys/resource.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#include <fstream>
#include <iostream>
#include <unordered_map>

#include "result.h"
#include "util.h"

namespace xenon {
namespace jail {
namespace {
void write_map_file(util::fs::path path,
                    const std::unordered_map<int, int>& map) {
  try {
    std::ofstream outp(path);
    outp.exceptions(std::ofstream::failbit | std::ofstream::badbit);
    for (auto& [from, to] : map) {
      outp << from << " " << to << " " << 1 << "\n";
    }
    outp.close();
  } catch (const std::exception& exc) {
    throw jail_error("failed to write uid_map");
  }
}
}  // namespace

void switch_users(const jail_options& opts,
                  const container_context& ctx,
                  bool ignore_groups) {
  if (!ignore_groups) {
    // Dropping groups is not allowed in a user namespace
    std::array<gid_t, 1> groups = {(gid_t)ctx.run_as_user_gid};
    check_sys(setgid(ctx.run_as_user_gid), "failed to switch user group");
    // just setting gid does not drop extra groups, so we explicitly set one
    check_sys(setgroups(groups.size(), groups.data()),
              "failed to set user groups");
  }
  // must drop root last, otherwise we lose privs to set groups
  check_sys(setuid(ctx.run_as_user_uid), "failed to switch user");

  bool allow_root = opts.new_user_namespace && ctx.run_as_user_uid == 0;
  if ((geteuid() == 0 || getegid() == 0) && !allow_root) {
    // just in case, bail if we didn't drop root effectively
    throw jail_error("error: failed to drop root");
  }
}

jail_container::jail_container(const jail_options& options,
                               const container_context& context)
    : opts(options), ctx(context) {}

void jail_container::setup() {
  setup_user_namespace();
  setup_mount_namespace();
  setup_network_namespace();
  setup_uts_namespace();
  set_umask();
  put_self_in_cgroups();
  setup_chroot();
  switch_users();
  setup_rlimits();
  forbid_new_privs();
  setup_file_redirections();
  setup_seccomp();
  unblock_all_signals();
}

void jail_container::exec(const std::vector<std::string>& program_args) {
  std::vector<const char*> c_args;
  for (auto& arg : program_args) {
    c_args.push_back(arg.c_str());
  }
  c_args.push_back(nullptr);

  // Clear out the environment so that potentially sensitive values
  // are not leaked into the child process on accident
  std::vector<std::string> env = {
      "USER=" + opts.run_as_user,
      "PWD=" + opts.workdir,
      "PATH=/bin:/usr/bin:/usr/local/bin",
  };
  for (auto& var : opts.env) {
    env.push_back(var);
  }

  std::vector<const char*> c_env;
  for (auto& var : env) {
    c_env.push_back(var.c_str());
  }
  c_env.push_back(nullptr);

  int ret = execvpe(program_args[0].c_str(), (char* const*)c_args.data(),
                    (char* const*)c_env.data());
  check_sys(ret, "failed to execute " + program_args[0]);
  std::terminate();
}

void jail_container::mount_proc() {
  util::fs::create_directories("/proc");
  check_sys(mount("proc", "/proc", "proc", 0, nullptr),
            "failed to remount /proc");
}

void jail_container::mount_tmp() {
  util::fs::create_directories("/tmp");
  check_sys(mount("tmpfs", "/tmp", "tmpfs", 0, nullptr),
            "failed to mount tmpfs on /tmp");
}

void jail_container::setup_user_namespace() {
  if (!opts.new_user_namespace) {
    return;
  }
  if (!opts.new_pid_namespace) {
    check_sys(unshare(CLONE_NEWUSER), "failed to make user namespace");
  }
  int pid = getpid();
  check_sys(pid, "failed to get container pid");
  util::fs::path proc = "/proc/self";

  write_map_file(proc / "uid_map", {{ctx.run_as_user_uid, ctx.parent_euid}});
}

void jail_container::setup_mount_namespace() {
  if (!opts.new_mount_namespace) {
    return;
  }

  check_sys(unshare(CLONE_NEWNS), "failed to make new fs namespace");
  check_sys(mount("none", "/", nullptr, MS_REC | MS_PRIVATE, nullptr),
            "failed to set / as private");
  if (opts.new_pid_namespace && opts.chroot.empty()) {
    mount_proc();
  }
}

void jail_container::setup_network_namespace() {
  if (opts.new_network_namespace) {
    check_sys(unshare(CLONE_NEWNET), "failed to make new network namespace");
  }
}

void jail_container::setup_uts_namespace() {
  if (!opts.new_uts_namespace) {
    return;
  }

  check_sys(unshare(CLONE_NEWUTS), "failed to make new uts namespace");
  check_sys(sethostname(opts.hostname.c_str(), opts.hostname.size()),
            "failed to set hostname");
}

void jail_container::set_umask() {
  check_sys(umask(opts.umask), "failed to set umask");
}

void jail_container::put_self_in_cgroups() {
  auto pid = getpid();
  check_sys(pid, "failed to get jail pid");
  if (ctx.pid_cg) {
    ctx.pid_cg.value().insert_task(pid);

    // Reset cgroups so they are not deleted on exit
    ctx.pid_cg.value().detach();
    ctx.pid_cg.reset();
  }
  if (ctx.memory_cg) {
    ctx.memory_cg.value().insert_task(pid);

    ctx.memory_cg.value().detach();
    ctx.memory_cg.reset();
  }
}

void jail_container::setup_chroot() {
  if (opts.chroot.empty()) {
    if (!opts.workdir.empty()) {
      check_sys(chdir(opts.workdir.c_str()), "failed to cd to " + opts.workdir);
    }
    return;
  }

  if (!opts.chroot_overlay_layer.empty()) {
    setup_overlayfs();
  } else if (!opts.chroot_overlay_workdir.empty() ||
             !opts.chroot_overlay_write_dir.empty()) {
    throw jail_error(
        "chroot_overlay_workdir and chroot_overlay_write_dir cannot be set "
        "without any base layer in chroot_overlay_layer");
  }

  for (auto& mkdir : opts.chroot_mkdir) {
    util::fs::create_directories(mkdir);
  }

  for (auto& link_file : opts.chroot_link) {
    auto joined_path = util::path_join(opts.chroot, link_file.second);

    check_sys(link(link_file.first.c_str(), joined_path.c_str()),
              "failed to create hard link");
  }

  for (auto& mount_dirs : opts.chroot_bind) {
    auto joined_path = util::path_join(opts.chroot, mount_dirs.second);
    util::fs::create_directories(joined_path);

    check_sys(mount(mount_dirs.first.c_str(), joined_path.c_str(), nullptr,
                    MS_BIND, nullptr),
              "failed to bind mount " + mount_dirs.first);
    check_sys(mount("none", joined_path.c_str(), nullptr,
                    MS_REMOUNT | MS_BIND | MS_RDONLY, nullptr),
              "failed to remount readonly " + mount_dirs.first);
  }
  for (auto& mount_dirs : opts.chroot_bind_shared) {
    auto joined_path = util::path_join(opts.chroot, mount_dirs.second);
    util::fs::create_directories(joined_path);
    check_sys(mount(mount_dirs.first.c_str(), joined_path.c_str(), nullptr,
                    MS_BIND, nullptr),
              "failed to bind mount " + mount_dirs.first);
  }

  check_sys(chroot(opts.chroot.c_str()), "failed to chroot to " + opts.chroot);
  if (opts.workdir.empty()) {
    // Must change directory after chroot, to avoid any pointers to the outside
    check_sys(chdir("/"), "failed to cd into chroot");
  } else {
    // chdir() is now relative to the new root
    check_sys(chdir(opts.workdir.c_str()), "failed to cd to " + opts.workdir);
  }
  if (opts.chroot_mount_proc) {
    mount_proc();
  }
  if (opts.chroot_mount_tmp) {
    mount_tmp();
  }
}

void jail_container::setup_overlayfs() {
  if (opts.chroot.empty()) {
    throw jail_error("chroot must not be specified with chroot_overlay_layer");
  }
  if (!opts.chroot_overlay_write_dir.empty() &&
      opts.chroot_overlay_workdir.empty()) {
    throw jail_error(
        "chroot_overlay_workdir must be added for temporary space for "
        "chroot_overlay_write_dir");
  }
  if (!util::fs::is_directory(opts.chroot)) {
    util::fs::create_directories(opts.chroot);
  }

  std::string options = "lowerdir=";
  options += util::join_on(opts.chroot_overlay_layer, ":");
  if (!opts.chroot_overlay_write_dir.empty()) {
    options += ",upperdir=" + opts.chroot_overlay_write_dir;
    options += ",workdir=" + opts.chroot_overlay_workdir;
  }
  check_sys(
      mount("overlay", opts.chroot.c_str(), "overlay", 0, options.c_str()),
      "failed to setup overlayfs");
}

void jail_container::setup_rlimits() {
  auto set_limit = [&](int resource, long soft_limit,
                       std::optional<long> hard_limit = {}) {
    struct rlimit limit;
    limit.rlim_cur = soft_limit;
    limit.rlim_max = hard_limit.value_or(soft_limit);

    check_sys(setrlimit(resource, &limit), "failed to set rlimits");
  };

  if (opts.rlimit_cpu_sec) {
    set_limit(RLIMIT_CPU, opts.rlimit_cpu_sec.value(),
              opts.rlimit_cpu_sec.value() + 1);
  }
  if (opts.rlimit_address_space_mb) {
    set_limit(RLIMIT_AS, opts.rlimit_address_space_mb.value() * 1024 * 1024);
  }
  if (opts.rlimit_num_procs) {
    set_limit(RLIMIT_NPROC, opts.rlimit_num_procs.value());
  }
  if (opts.rlimit_num_fds) {
    set_limit(RLIMIT_NOFILE, opts.rlimit_num_fds.value());
  }
  if (opts.rlimit_fsize_kb) {
    set_limit(RLIMIT_FSIZE, opts.rlimit_fsize_kb.value() * 1024);
  }
  if (opts.rlimit_core_size_kb) {
    set_limit(RLIMIT_CORE, opts.rlimit_core_size_kb.value() * 1024);
  }
}

void jail_container::switch_users() {
  ::xenon::jail::switch_users(opts, ctx,
                              /* ignore_groups */ opts.new_user_namespace);
}

void jail_container::forbid_new_privs() {
  if (!opts.forbid_new_privs) {
    return;
  }
  // NO_NEW_PRIVS prevents any child from gaining more privs than we currently
  // have, even in the presence of setuid binaries.
  check_sys(prctl(PR_SET_NO_NEW_PRIVS, 1, 0, 0, 0), "failed to call prctl");
  // SET_DUMPABLE to 0 prevents coredumps
  check_sys(prctl(PR_SET_DUMPABLE, 0, 0, 0, 0), "failed to call prctl");
}

void jail_container::setup_seccomp() {
  if (ctx.seccomp_policy) {
    ctx.seccomp_policy.value().apply();
  }
}

void jail_container::setup_file_redirections() {
  auto dup_and_close = [](auto& fd, int target) {
    if (fd) {
      check_sys(dup2(fd.value(), target), "dup2 failed");
      check_sys(close(fd.value()), "failed to close fd");
    }
  };

  dup_and_close(ctx.stdin_fd, STDIN_FILENO);
  dup_and_close(ctx.stdout_fd, STDOUT_FILENO);
  dup_and_close(ctx.stderr_fd, STDERR_FILENO);
}

void jail_container::unblock_all_signals() {
  sigset_t signals;
  sigfillset(&signals);
  check_sys(sigprocmask(SIG_UNBLOCK, &signals, nullptr),
            "failed to unblock signals");
}

}  // namespace jail
}  // namespace xenon
