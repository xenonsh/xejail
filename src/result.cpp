#include "result.h"

#include <sys/resource.h>
#include <sys/time.h>

#include <cstring>

#include "cgroup.h"

namespace xenon {
namespace jail {

void check_sys(int retn, const std::string& message) {
  if (retn < 0) {
    auto error = std::string(std::strerror(errno));
    throw jail_error(message + ": " + error);
  }
}

std::string jail_status_to_string(jail_status status) {
  switch (status) {
    case jail_status::NONE:
      return "none";
    case jail_status::OK:
      return "ok";
    case jail_status::EXIT_ERROR:
      return "exit_error";
    case jail_status::EXIT_SIGNAL:
      return "exit_signal";
    case jail_status::CPU_TIME_EXCEEDED:
      return "cpu_time_exceeded";
    case jail_status::WALL_TIME_EXCEEDED:
      return "wall_time_exceeded";
    case jail_status::MEMORY_LIMIT_EXCEEDED:
      return "memory_limit_exceeded";
    case jail_status::OUTPUT_LIMIT_EXCEEDED:
      return "output_limit_exceeded";
    case jail_status::BAD_SYSCALL:
      return "bad_syscall";
    case jail_status::SETUP_ERROR:
      return "setup_error";
  }
  std::terminate();
}

void jail_result::record_stats(std::chrono::steady_clock::time_point start_time,
                               std::optional<memory_cgroup>& mem_cg) {
  wall_time = std::chrono::duration_cast<std::chrono::milliseconds>(
      std::chrono::steady_clock::now() - start_time);

  rusage usage;
  check_sys(getrusage(RUSAGE_CHILDREN, &usage),
            "failed to get child resource usage");

  sys_cpu_time = std::chrono::milliseconds(usage.ru_stime.tv_sec * 1000 +
                                           usage.ru_stime.tv_usec / 1000);
  usr_cpu_time = std::chrono::milliseconds(usage.ru_utime.tv_sec * 1000 +
                                           usage.ru_utime.tv_usec / 1000);
  cpu_time = sys_cpu_time + usr_cpu_time;
  max_rss_used_mb = usage.ru_maxrss / 1024;

  if (mem_cg) {
    cgroup_memory_usage_mb = mem_cg.value().get_max_usage_mb();
    cgroup_memory_alloc_failures = mem_cg.value().get_fail_count();
  }
}

void jail_result::dump(std::ostream& stream) {
  stream << "status: " << jail_status_to_string(status) << std::endl;
  if (exit_code) {
    stream << "exit_code: " << exit_code.value() << std::endl;
  }
  if (exit_signal) {
    std::string signame(strsignal(exit_signal.value()));
    stream << "exit_signal: '" << signame << "'" << std::endl;
  }
  if (abort_message) {
    stream << "abort_message: '" << abort_message.value() << "'" << std::endl;
  }

  stream << "wall_time_ms: " << wall_time.count() << std::endl;
  stream << "cpu_time_ms: " << cpu_time.count() << std::endl;
  stream << "usr_cpu_time_ms: " << usr_cpu_time.count() << std::endl;
  stream << "sys_cpu_time_ms: " << sys_cpu_time.count() << std::endl;
  stream << "max_rss_used_mb: " << max_rss_used_mb << std::endl;

  if (cgroup_memory_usage_mb) {
    stream << "cgroup_memory_usage_mb: " << cgroup_memory_usage_mb.value()
           << std::endl;
  }
  if (cgroup_memory_alloc_failures) {
    stream << "cgroup_memory_alloc_failures: "
           << cgroup_memory_alloc_failures.value() << std::endl;
  }
}
}  // namespace jail
}  // namespace xenon