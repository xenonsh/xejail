#include "cgroup.h"
#include "options.h"
#include "seccomp.h"

namespace xenon {
namespace jail {

/*
 * Shared container context between the supervisor (parent process)
 * and the containerized process (child).
 */
struct container_context {
  std::optional<int> stdin_fd;
  std::optional<int> stdout_fd;
  std::optional<int> stderr_fd;

  int run_as_user_uid{-1};
  int run_as_user_gid{-1};
  int parent_euid{-1};
  int parent_egid{-1};

  std::optional<memory_cgroup> memory_cg;
  std::optional<pid_cgroup> pid_cg;

  std::optional<seccomp_policy> seccomp_policy;
};

/*
 * TODO: comment
 */
void switch_users(const jail_options& options,
                  const container_context& ctx,
                  bool ignore_groups = false);

/*
 * Operations on a containerized process (the child process) that are done
 * post-fork. Sets up:
 *  - Linux namespaces (mount, network, UTS / "hostname")
 *  - chroots and bindmounts
 *  - Inserts self into cgroups (created by parent, so parent can monitor)
 *  - Sets up rlimits
 *  - Drops root to the specified user, and forbids gaining root again
 *  - Applies seccomp policies, if appropriate
 *  - Redirects stdio to files, if appropriate
 *
 * Then, finally execs the target program.
 */
class jail_container {
 public:
  jail_container(const jail_options& options, const container_context& context);

  /*
   * Apply all container policies to the current process, to be done before
   * executing the target program.
   */
  void setup();
  /*
   * Executes the target program (from program_args[0]) with the given
   * arguments. This function does not return normally -- if setup or the exec
   * syscall fails this function will throw.
   */
  void exec(const std::vector<std::string>& program_args);

 private:
  // Sets up a user namespace, which maps UIDs (and GIDs) in the container
  // to ones in the host.
  void setup_user_namespace();
  // Set up a new network namespace (if configured), which prevents the
  // jailed program from interacting with any network on the host. The
  // process will only see an isolated loopback device.
  void setup_network_namespace();
  // Remount /proc, if configured. Inside a PID namespace /proc will
  // view the new namespace -- only seeing processes inside this namespace
  // along with their mapped PIDs.
  void mount_proc();
  // Mount /tmp as a new tmpfs inside a chroot, if configured.
  void mount_tmp();
  // Setup a new mount namespace if configured. This prevents mounts
  // (particularly bind-mounts for chroots) from effecting the outside world,
  // and also allowing the kernel to deallocate the mounts automatically on
  // process exit.
  void setup_mount_namespace();
  // Sets up a new "UTS" namespace if configured. Practicallly this allows
  // setting a unique hostname for the jailed process that does not effect the
  // outside. See --hostname.
  void setup_uts_namespace();

  // Sets the default umask (see `man umask`) for the target program.
  void set_umask();

  // Puts the current process in the given cgroups, if configured, by writing
  // the PID to their tasks files.
  void put_self_in_cgroups();

  // Setup a chroot for the target program, optionally bind-mounting
  // any configured directories from the host or linking in files.
  // Note that either the chroot needs to be prepopulated or binaries
  // must be mounted in otherwise there will be nothing to run.
  void setup_chroot();
  // If configured, setup the chroot as an overlayfs: a new mount composed
  // of base layers to form an image.
  void setup_overlayfs();
  // Set any configured resouce limits with setrusage. Note that these mostly
  // apply to single processes (except RLIMIT_NPROC), so cgroups are needed
  // if fork() is allowed.
  void setup_rlimits();

  // Switch users and groups to user (and default group of that user) specified
  // in options. Ensures that both euid and egid are no longer root.
  void switch_users();
  // Forbids the containerized program from gaining new privileges. For example,
  // prevents it from gaining root through a setuid program or from ptrace()
  // on such a program.
  void forbid_new_privs();

  // Applies any specified seccomp policies, see seccomp.h
  void setup_seccomp();

  // Redirect stdio as necessary, and close all opened file descriptors.
  void setup_file_redirections();
  // Unblock any signals that were blocked (or inherited from parents) such
  // that the child has a clean set.
  void unblock_all_signals();

 private:
  jail_options opts;
  container_context ctx;
};

}  // namespace jail
}  // namespace xenon