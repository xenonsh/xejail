#include <seccomp.h>

#include <istream>
#include <optional>
#include <vector>

namespace xenon {
namespace jail {

/*
 * A single rule for seccomp-BPF, see seccomp_policy.
 *
 * Rules combine a predicate on a syscall with an action to take (such as
 * KILL, LOG, or ALLOW). Only one syscall can be specified per rule (by number),
 * but additional predicates may be made on the arguments to the syscall. Note
 * that BPF only allows simple predicates and does not allow pointer
 * dereferences so string handling is not possible.
 */
struct seccomp_rule {
  int syscall{-1};
  uint32_t action{SCMP_ACT_KILL};

  std::vector<scmp_arg_cmp> arg_cmps;
};

/*
 * A policy for handling syscalls using seccomp-bpf (see
 * https://www.kernel.org/doc/Documentation/prctl/seccomp_filter.txt).
 *
 * seccomp-bpf allows blocking or otherwise handling syscalls by number.
 * This lets us prevent jailed programs from using restricted syscalls
 * (e.g. socket() or connect()), even based on their argument values
 * (e.g. prevent open() calls that do not specify O_RDONLY). The application
 * of policies here is a wrapper around libseccomp to avoid dealing with BPF
 * programs by hand.
 *
 * seccomp_policy::load() provides a parser for a small DSL for building
 * seccomp-bpf policies. Rules are specified with simple tokens like "allow
 * close". See seccomp_policies.cpp for examples.
 */
class seccomp_policy {
 public:
  /*
   * Parse a seccomp-bpf policy from a given input stream as a DSL. Format is
   * intentionally simple, and is parsed only with the stream tokenizer.
   *
   * Format for DSL:
   *  - A default action is specified with "default $action" where $action is
   *    one of "allow", "die", "error $errno", or "log".
   *  - Rules can be specified with actions per-syscall such as "allow close"
   *    or "error EACCES close".
   *  - Simple predicates on arguments can be specified as
   *    "if [ $1 == 0 ] allow close"
   */
  static seccomp_policy load(std::istream& stream);
  /*
   * Load a policy from a string, same format as ::load(std::ifstream).
   */
  static seccomp_policy load(const std::string& value);
  /*
   * Get a predefined seccomp policy. See seccomp_policies.cpp for definitions.
   *
   * Returns none if `name` does not refer to a built in policy.
   */
  static std::optional<seccomp_policy> get_named(const std::string& name);

  /*
   * Apply the current policy to the running program.
   *
   * Preferably done just prior to running untrusted code, otherwise may block
   * syscalls that are needed for further sandboxing.
   *
   * Throws if the policy cannot be applied
   */
  void apply();

  uint32_t default_action{SCMP_ACT_KILL};
  std::vector<seccomp_rule> rules;
};

/*
 * Generic typed exception thrown when parsing the seccomp policy language
 * fails, including some context. Useful for typed-catch blocks.
 */
class policy_parse_error : public std::exception {
 public:
  explicit policy_parse_error(std::istream& stream, std::string msg)
      : message("failed to parse seccomp policy, pos " +
                std::to_string(stream.tellg()) + ": " + std::move(msg)) {}

  const char* what() const noexcept override { return message.c_str(); }

 private:
  std::string message;
};
}  // namespace jail
}  // namespace xenon