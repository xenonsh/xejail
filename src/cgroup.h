#include <optional>

#include "options.h"
#include "util.h"

namespace xenon {
namespace jail {

/*
 * Wrapper around filesystem operations to manipulate Linux cgroups.
 *
 * cgroups allow for accounting and restrictions on multiple processes,
 * such as limiting memory usage, number of processes, cpu, etc.
 *
 * cgroups are configured with a file system interface, most commonly mounted
 * at /sys/fs/cgroup with several "controllers" as subdirectories
 * (e.g. /sys/fs/cgroup/memory for the memory controller).
 *
 * Creating a new cgroup for a given controller is as simple as creating a
 * directory /sys/fs/cgroup/$controller/$name. Processes can be added by
 * writing their pid to the "tasks" file in this directory, and additional
 * configuration can be done by writing to other files. Accounting is done
 * by reading from stats files.
 */
class cgroup {
 public:
  /*
   * Create a new cgroup at a given mount point with the given name,
   * i.e. $mount_point/$name. This directory will be removed when the
   * cgroup object is destroyed.
   */
  cgroup(util::fs::path mount_point, std::string name);
  ~cgroup();

  /*
   * Detach this cgroup object from the given directory, such that
   * destruction of the cgroup object will not attempt to remove the
   * cgroup directory.
   */
  void detach();

  /*
   * Insert a process (by PID) into this cgroup by writing to the tasks file.
   * Child processes of the given PID will be contained automatically.
   */
  void insert_task(int pid);
  /*
   * Send SIGKILL to all processes contained in this cgroup.
   */
  void kill_all_grouped();

 protected:
  util::fs::path cg_path;
};

/*
 * Wrapper for the memory cgroup, commonly mounted at /sys/fs/cgroup/memory,
 * with helpers for restrictions and accounting. Note this is based
 * on cgroupsv1.
 *
 * See https://www.kernel.org/doc/Documentation/cgroup-v1/memory.txt
 */
class memory_cgroup : public cgroup {
 public:
  memory_cgroup(util::fs::path mount_point, std::string name);

  /*
   * Set the memory limit for the whole cgroup by writing
   * memory.limit_in_bytes.
   */
  void set_memory_limit_mb(int limit_in_mb);
  /*
   * Disable the kernel OOM-killer for the processes in the cgroup. If the
   * cgroup runs out of the memory processes will stall instead of being
   * terminated. See listen_for_oom(eventfd).
   */
  void disable_oom_killer();
  /*
   * Lower swapiness of the cgroup to 0. Note that this does not totally prevent
   * paging to disk. See cgroup documentation.
   */
  void disable_swap();
  /*
   * Listen for OOM-events with a given eventfd by writing to
   * memory.oom_control.
   */
  void listen_for_oom(int eventfd);

  /*
   * Get the maximum memory usage (over the lifetime of the cgroup) in
   * megabytes, from memory.max_usage_in_bytes.
   */
  long get_max_usage_mb();
  /*
   * Get the number of memory allocation failures (over the lifetime of the
   * cgroup), used for OOM heuristics.
   */
  long get_fail_count();
};

/*
 * cgroup which limits PIDs as a resource, commonly mounted at
 * /sys/fs/cgroup/pids/. Note that this cgroup limits by blocking calls to
 * clone and fork, and the PID limit applies both to "processes" as well
 * as threads.
 *
 * See https://www.kernel.org/doc/Documentation/cgroup-v1/pids.txt
 */
class pid_cgroup : public cgroup {
 public:
  pid_cgroup(util::fs::path mount_point, std::string name);
  void set_pid_limit(int pids);
};

}  // namespace jail
}  // namespace xenon