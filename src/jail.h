#pragma once

#include <atomic>
#include <chrono>
#include <iostream>
#include <optional>

#include "container.h"
#include "eventloop.h"
#include "options.h"
#include "result.h"

namespace xenon {
namespace jail {

/*
 * Setup and run a program inside a "jail". A set of `jail_options`
 * define a setup for using common Linux utilities to limit the
 * functionality of a subprocess (preventing it from forking or using
 * certain syscalls) as well as limit the resource usage (wall time, cpu time,
 * memory, etc).
 *
 * The jailed program is run in a forked subprocess, which is monitored
 * by xejail. For post-fork setup in the jailed child process, see container.h.
 * Primarily, the parent creates any shared resources necessary (opens file
 * descriptors and sets up cgroups) and monitors any events for child processes,
 * as well as attaching to the child using ptrace (see trace.h).
 */
class jail_program {
 public:
  /*
   * Create a new PID namespace, fork a child that runs in that namespace.
   *
   * This function only returns _inside the PID namespace. The current
   * process (before the call) is turned into a monitor that just waits
   * for the PID namespace to exit.
   *
   * This setup is necessary for correct signal handling (since init, even
   * of a PID namespace, ignores signals that it does not have a signal handler
   * for). See usage in main.cpp.
   */
  static void fork_monitor_for_pid_namespace(const jail_options& opts);

  /*
   * Create a jail_program from the given options and target program
   * arguments.
   *
   * May fail if actualizing some of the options fails. For example, opts
   * may refer to an invalid user or an input file that does not exist.
   * In this case a jail_error will be thrown.
   */
  static jail_program from_opts(const jail_options& opts,
                                const std::vector<std::string>& args);

  /*
   * Setup the jail and run the target program. This blocks the current
   * process until the jail completes or is terminated for some reason.
   *
   * Returns resource usage and stats in the result. See result.h.
   */
  jail_result run();

  /*
   * Reference to the context for the container that will be setup.
   */
  const container_context& context();

 private:
  explicit jail_program(const jail_options& jail_opts,
                        const std::vector<std::string>& prog_args)
      : opts(jail_opts), program_args(prog_args) {}

  /*
   * Create cgroup (if necessary from options) directories. Does not
   * place any processes into the newly created cgroups yet, as the
   * child will insert itself.
   */
  void setup_cgroups();
  /*
   * Check the memory cgroup to see if the memory limit was exceeded.
   * We want to report MEMORY_LIMIT_EXCEEDED if it is likely the jailed
   * program failed due the memory limit. Unfortunately, due to cgroup
   * semantics, a program which is approaching the memory limit often
   * does not crash cleanly. cgroups behave more like a whole host
   * hitting memory pressure, so the kernel will page aggressively,
   * often slowing down the program and causing other issues.
   *
   * We apply a heuristic to attempt to detect these cases based on
   * memory allocation failures (from the memory cgroup) and report
   * MEMORY_LIMIT_EXCEEDED.
   */
  void check_for_memory_exceeded();

  /*
   * Setup the necessary functionality inside the child process
   * and execute the target program.
   */
  void run_child();

  /*
   * Update the result with a given status and message.
   */
  void record_result(jail_status status, std::string message = "");
  /*
   * Forcibly kill the jailed program (if it is still running) and any
   * subprocesses it may have, and update the results.
   */
  void abort_container(jail_status status, std::string message = "");
  /*
   * Same as abort_container in functionality, but for subprocess
   * which is trapped from ptrace(). In this case, we must continue the
   * process (with PTRACE_CONT) but deliver a kill signal, otherwise
   * the may still be trapped and not gete the signal.
   */
  void abort_trapped_container_or_child(int pid,
                                        jail_status status,
                                        std::string message);
  /*
   * Cleanup any resources necessary in the parent process before
   * blocking on child completion.
   */
  void cleanup_in_parent();
  /*
   * Handle a ptrace event from a trapped child `pid`.
   *
   * `event` corresponds to some PTRACE_EVENT_* value (see man ptrace).
   */
  void handle_ptrace_event(int pid, int ptrace_event);
  /*
   * Handle any available status updates from child processes (via waitpid()),
   * including termination or ptrace traps.
   *
   * Returns true if the jailed program has terminated and monitoring
   * should stop, false if not.
   */
  bool reap_child_events();
  /*
   * Begin monitoring the child process for completion and handling
   * traps. Blocks until child completes or is otherwise terminated.
   */
  void monitor_child(int pid);

 private:
  jail_options opts;
  jail_result result;

  event_loop loop;

  container_context ctx;
  std::vector<std::string> program_args;

  std::chrono::steady_clock::time_point start_time{
      std::chrono::steady_clock::now()};
  std::optional<int> jail_pid;

  bool has_done_exec{false};
};
}  // namespace jail
}  // namespace xenon