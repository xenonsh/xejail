#pragma once

#include <algorithm>
#include <experimental/filesystem>
#include <optional>
#include <string>
#include <unordered_set>
#include <vector>

namespace xenon {
namespace jail {
namespace util {

// Reference std::filesystem here, so pointer can be changed
// once it is no longer "experimental". Note that usage requires
// -lstdc++fs.
namespace fs = std::experimental::filesystem;

/*
 * Parse a string to an integer in the given base (using std::stoi).
 * Returns none if value cannot be parsed.
 */
std::optional<int> to_int(const std::string& value, size_t base = 10);

/*
 * Split `value` given the delimeter `delim` into two strings. If
 * `value` does not contain any instances of `delim` returns none.
 * Else returns a pair of value[:i] and value[i + 1:] where `i` is
 * the first index of `delim` in `value`.
 */
std::optional<std::pair<std::string, std::string>> split_on(
    const std::string& value,
    char delim);
/*
 * Concatenate a list of strings into a single one with consecutive values
 * being separated by `delim`.
 */
std::string join_on(const std::vector<std::string>& strings, std::string delim);

/*
 * Generic contains function for a container, based on
 * container::begin(), container::end() and std::find().
 */
template <typename container_t, typename T>
bool contains(const container_t& container, const T& value) {
  auto it = std::find(container.begin(), container.end(), value);
  return it != container.end();
}

/*
 * Attempt to close all open file descriptors for this process
 * which are not a part of `valid_fds`. This can be used to avoid
 * leaking file descriptors to children where not intended (e.g.
 * in case the calling process of xejail had open file descriptors).
 *
 * The open file descriptor set is read from /proc/self/fd, so /proc
 * must be available.
 */
void try_close_all_other_fds(const std::unordered_set<int>& valid_fds);

/*
 * Join two paths together with the native path separator. Simple wrapper
 * around fs::path::operator/, but implicit conversions from string make this
 * clearer to use in the case that you have two string types.
 */
fs::path path_join(const fs::path& lhs, const fs::path& rhs);
}  // namespace util
}  // namespace jail
}  // namespace xenon