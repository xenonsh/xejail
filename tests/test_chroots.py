import os
from utils import run_jail, tempdir


basic_chroot_binds = [
    "/bin/",
    "/usr/bin/",
    "/usr/lib/",
    "/usr/local/include/",
    "/usr/include/",
    "/etc/",
    "/lib/",
    "/lib64/",
    "/lib32/",
]


def test_basic_chroot():
    with tempdir() as chroot_dir:
        result = run_jail(['ls'], chroot=chroot_dir,
                          chroot_bind=basic_chroot_binds)
        assert(result.was_ok())
        assert("etc" in result.stdout)
        assert("lib64" in result.stdout)
        assert("tmp" not in result.stdout)

        runpath = os.path.join(chroot_dir, 'run')
        os.mkdir(runpath)
        os.chmod(runpath, 0777)

        result = run_jail(['touch', 'abc'], chroot=chroot_dir,
                          chroot_bind=basic_chroot_binds, workdir='/run')

        assert(result.was_ok())
        assert(os.path.isfile(os.path.join(runpath, 'abc')))


def test_chroot_mount_proc():
    with tempdir() as chroot_dir:
        result = run_jail(['ls', '/proc'], chroot=chroot_dir,
                          chroot_bind=basic_chroot_binds)
        assert(not result.was_ok())

        result = run_jail(['cat', '/proc/self/status'], chroot=chroot_dir,
                          chroot_bind=basic_chroot_binds, chroot_mount_proc='t')
        assert(result.was_ok())
        assert('Pid:' in result.stdout)

