import utils
from utils import run_python_code, run_jail


def test_wall_time_limit():
    result = run_jail(['sleep', '10'], wall_time_limit_ms=200)
    assert(not result.was_ok())
    assert(result.results['status'] == 'wall_time_exceeded')


def test_cpu_limit():
    result = utils.run_python_test_program('busy_thread.py', rlimit_cpu_sec=1)
    assert(not result.was_ok())
    assert(result.results['status'] == 'cpu_time_exceeded')
    assert(700 <= result.results['wall_time_ms'] <= 2000)
    assert(700 <= result.results['cpu_time_ms'] <= 2000)

    result = utils.run_python_test_program(
        'busy_2_threads.py', rlimit_cpu_sec=1)
    assert(not result.was_ok())
    assert(result.results['status'] == 'cpu_time_exceeded')
    assert(700 <= result.results['wall_time_ms'] <= 2000)
    assert(700 <= result.results['cpu_time_ms'] <= 2000)


def test_rlimit_memory():
    result = utils.run_python_test_program(
        'use_memory_alot.py', rlimit_address_space_mb=256)
    assert(not result.was_ok())
    # Without cgroups, we don't know that memory limits were hit,
    # so the program just fails
    assert('MemoryError' in result.stderr)


def test_rlimit_procs():
    result = run_python_code('''
import os
os.fork()
os.fork()
os.fork()
''',
        allow_fork='t',
        rlimit_num_procs=2,
    )
    assert(not result.was_ok())

