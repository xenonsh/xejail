from utils import run_binary_program, run_jail


def test_abort_and_segfault():
    result = run_binary_program('abort')
    assert(not result.was_ok())
    assert(result.results['status'] == 'exit_signal')
    assert('abort' in result.results['exit_signal'].lower())

    result = run_binary_program('segfault')
    assert(not result.was_ok())
    assert(result.results['status'] == 'exit_signal')
    assert('segm' in result.results['exit_signal'].lower())