import os
import yaml
import shutil
import time
import tempfile
import traceback

from contextlib import contextmanager
from collections import namedtuple
from subprocess import Popen, PIPE
from tempfile import NamedTemporaryFile


class TestContext(object):
    def __init__(self):
        self.jail_binary = None
        self.current_test = None
        self.test_results = []

    def start_test(self, name):
        self.current_test = name

    def finish_test(self, result):
        self.test_results.append((self.current_test, result))
        self.current_test = None


test_context = TestContext()
test_context.jail_binary = os.environ.get('JAIL_BINARY', '').split()

assert os.geteuid() == 0, "tests should be run as root"
assert(test_context.jail_binary)


class JailResult(object):
    def __init__(self, result_file, stdout, stderr):
        self.stdout = stdout
        self.stderr = stderr
        with open(result_file) as fd:
            self.results = yaml.load(fd)
            if self.results is None:
                raise Exception('jail did not produce results')

    def was_ok(self):
        return self.results.get('status') == 'ok'


def run_jail(
    command,
    **options
):
    if options is None:
        options = {}
    with NamedTemporaryFile() as result_file:
        options['result_file'] = result_file.name
        opt_args = []
        for param, val in options.items():
            if type(val) is bool:
                opt_args.append('{}={}'.format(param, 't' if val else 'f'))
            elif type(val) is list:
                for arg in val:
                    opt_args.append('{}={}'.format(param, arg))
            else:
                opt_args.append('{}={}'.format(param, val))
        jail_command = test_context.jail_binary + \
            opt_args + ['--'] + command

        proc = Popen(jail_command, stdout=PIPE, stderr=PIPE)
        stdout, stderr = proc.communicate(input=input)
        return JailResult(result_file.name, stdout, stderr)


def run_python_code(code_as_string, **options):
    return run_jail(['python', '-c', code_as_string], **options)


def run_python_test_program(filename, **options):
    cur_dir, _ = os.path.split(__file__)
    test_program_path = os.path.join(cur_dir, 'test_programs', filename)
    return run_jail(['python', test_program_path], **options)


def run_binary_program(filename, **options):
    cur_dir, _ = os.path.split(__file__)
    test_program_path = os.path.join(cur_dir, 'test_programs', filename)
    return run_jail([test_program_path], **options)


@contextmanager
def tempdir():
    tmpdir = tempfile.mkdtemp("-xejail-test")
    os.chmod(tmpdir, 0777)
    yield tmpdir
    shutil.rmtree(tmpdir)