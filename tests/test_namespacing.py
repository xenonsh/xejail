import re

from utils import run_jail, run_python_code


def test_basic_pid_namespace():
    result = run_python_code('import os; print(os.getpid())')
    assert(result.was_ok())
    assert(result.stdout.strip() == '2')

    result = run_python_code('import os; print(os.getpid())', new_pid_namespace='f')
    assert(result.was_ok())
    assert(result.stdout.strip() != '2')


def test_proc_remount():
    result = run_jail(['ps', 'aux'])
    assert(result.was_ok())
    lines = [line.strip() for line in result.stdout.split('\n') if line.strip()]
    assert(len(lines) == 3)
    assert(re.match(r'nobody\s+2', lines[-1]))
