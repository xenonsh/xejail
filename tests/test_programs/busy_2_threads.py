import threading


def spin():
    x = 2
    while True:
        x *= 3
        x /= 2
        x += 1

bg_thread_1 = threading.Thread(target=spin)
bg_thread_1.start()

bg_thread_2 = threading.Thread(target=spin)
bg_thread_2.start()

bg_thread_1.join()
bg_thread_2.join()