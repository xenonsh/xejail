import threading


def spin():
    x = 2
    while True:
        x *= 3
        x /= 2
        x += 1

bg_thread = threading.Thread(target=spin)
bg_thread.start()

bg_thread.join()