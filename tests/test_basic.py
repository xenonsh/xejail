import os

from utils import run_jail, run_python_code


def test_echo_hi():
    result = run_jail(['echo', 'hi'])
    assert(result.was_ok())
    assert(result.stdout.strip() == 'hi')


def test_no_fork_allowed():
    result = run_python_code('import os; os.fork()', allow_fork='t')
    assert(result.was_ok())

    result = run_python_code('import os; os.fork()')
    assert(not result.was_ok())
    assert(result.results['status'] == 'bad_syscall')


def test_no_exec_allowed():
    exec_cmd = ['bash', '-c', '/bin/ls']
    result = run_jail(exec_cmd, allow_fork='t', allow_exec='f')
    assert(not result.was_ok())
    assert(result.results['status'] == 'bad_syscall')


def test_drop_root():
    result = run_jail(['whoami'])
    assert(result.was_ok())
    assert(result.stdout.strip() == 'nobody')

    result = run_jail(['id'])
    assert(result.was_ok())
    assert('root' not in result.stdout)


def test_no_leak_env():
    os.environ['LEAK_THIS'] = '1'
    result = run_jail(['env'])
    assert(result.was_ok())
    assert('LEAK_THIS' not in result.stdout)


def test_add_env():
    result = run_jail(['env'], env='MY_VAR=TEST')
    assert(result.was_ok())
    assert('MY_VAR=TEST' in result.stdout)

    result = run_jail(['env'], env=['MY_VAR=TEST', 'MY_VAR2=TEST3'])
    assert(result.was_ok())
    assert('MY_VAR=TEST' in result.stdout)
    assert('MY_VAR2=TEST3' in result.stdout)


def test_umask():
    result = run_jail(['bash', '-c', 'umask'], umask='077')
    assert(result.was_ok())
    assert('077' in result.stdout)